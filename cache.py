"""Module for redis cache"""
from pandas import DataFrame
import pandas as pd
# import redis
from typing import Optional
import gc
import os

# cache: redis.Redis = redis.Redis(
#     host="localhost",
#     socket_timeout=300000000, 
#     socket_connect_timeout=300000000
# )

_cache_dir = "c"

def get_dataframe(key: str) -> Optional[DataFrame]: 
    file = os.path.join(_cache_dir, key)
    if not os.path.exists(file):
        return None
    with open(file, "rb") as f:
        raw_data = f.read()
        raw_data = None if raw_data is None \
                        else pd.read_msgpack(raw_data)

        gc.collect()
        return raw_data

def save_dataframe_in_cache(key: str, data: DataFrame): 
    file = os.path.join(_cache_dir, key)
    data.to_msgpack(file)
    gc.collect()

# def get_dataframe(key: str) -> Optional[DataFrame]: 
#     raw_data = cache.get(key)
#     raw_data = None if raw_data is None \
#                     else pd.read_msgpack(raw_data)

#     gc.collect()
#     return raw_data

# def save_dataframe_in_cache(key: str, data: DataFrame): 
#     cache.set(key, data.to_msgpack(compress='zlib'))
#     gc.collect()