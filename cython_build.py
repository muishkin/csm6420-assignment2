from distutils.core import setup
from Cython.Build import cythonize
import os

all_python_files = [os.path.join(dp, f) for dp, dn, filenames in os.walk(".") \
                      for f in filenames if f.endswith(".py")]

setup(name='ROB65 ML App',
      ext_modules=cythonize(all_python_files)
)