"""Module for importing signal data"""

from typing import Tuple, Optional, List, Dict, Iterable
import pandas as pd
from pandas import DataFrame
import numpy as np
from sklearn.preprocessing import scale
from sklearn.decomposition import PCA
import matplotlib.pyplot as pyplot
import gc
import pickle
import os
from datetime import datetime

from data_import.features import vectorise, split_features_and_outcomes, split_training_and_testing_data
from models.DataSet import DataSet
import cache

signal_train_file_import = "~/data/csm6420/train_signal.csv"
signal_test_file_import = "~/data/csm6420/test_signal.csv"

fft_n_components = 300

def _get_raw_data() -> DataFrame: 
    """Get raw data from file. Hold in memory as a cache.
    
    Returns:
        DataFrame -- Raw imported data.
    """
    raw_data = cache.get_dataframe("signal_data")
    if raw_data is None:
        raw_data = pd.read_csv(signal_train_file_import)
        cache.save_dataframe_in_cache("signal_data", raw_data)

    return raw_data

def get_ids_for_stratified_split() -> (np.array, np.array):
    """Return a list of all the raw ids in the Signal CSV file.
    
    Returns:
        (np.array, np.array) -- (ids, ys) for stratified split.
    """

    data = _get_raw_data()
    return (np.array(data["ID"]), np.array(data["Type"]))


def import_signal_validation(clip_values: Optional[Tuple[float, float]] = None,
                             perform_fft: bool = False,
                             pca_date_str: Optional[str] = None,
                             remove_incomplete_sequences: bool = True,
                             de_noise: bool = False
                            ) -> DataSet:
    """Import the signal data-set from file_path
    
    Keyword Arguments:
        clip_values {Optional[Tuple[Float, Float]]} -- Values to clip the X 
            values at. (default: {None})
        perform_fft {bool} -- Whether or not to perform a fast fourier transform
            on the data. (default: {False})
        pca_model_date_str {Optional[str]} -- Required if doing FFT transform.  
            Specified the location of the pickled PCA model.
        remove_incomplete_sequences {bool} -- Remove sequences which don't have
            the full 6000 data points.
        de_noise {bool} -- Whether to de-noise the signal using FFT and inverse.
    Returns:
        DataSet -- The imported and transformed data-set
    """
    data = pd.read_csv(signal_test_file_import, dtype={"ID": "str"})

    gc.collect()

    ids, X, y = split_features_and_outcomes(data)

    gc.collect()

    X = np.array(list([[i for i in x if np.isfinite(i) 
                                     and isinstance(i, float)] for x in X]))


    gc.collect()

    if clip_values is not None:
        X = np.array([np.clip(x, clip_values[0], clip_values[1]) for x in X])

    if perform_fft:
        X_fft = ([np.abs(np.fft.rfft(x)) for x in X]) 
        # Only take the first 300 components of the Frequency data
        # this seems to contain most of the information.
        X_fft = np.array([np.array(x[0:300]) for x in X_fft])

        load_path = os.path.join("models", 
                                 f"signal_pca_{pca_date_str}.sklearn")

        with open(load_path, 'rb') as f:
            pca: PCA = pickle.load(f)       

        X_fft = pca.transform(X_fft)

        return DataSet(ids, X_fft, y)

    if remove_incomplete_sequences:
        Xs_and_ys = [(i, x, y) for (i, x, y) in zip(ids, X, y) 
                        if len(x) == 6000]
        ids = np.array(list([i for (i, x, y) in Xs_and_ys]))
        X = np.array(list([x for (i, x, y) in Xs_and_ys]))
        y = np.array(list([y for (i, x, y) in Xs_and_ys]))

    if de_noise:
        # Let's de-noise the signal a bit.
        X_fft = [np.fft.rfft(x) for x in X]
        # Only take the first 300 components of the Frequency data
        # this seems to contain most of the information.
        X_fft = np.array([np.array(x[0:300]) for x in X_fft])

        X = [np.fft.irfft(x) for x in X_fft]

    X = np.array([np.array([np.array([xi]) for xi in x]) for x in X])
    gc.collect()

    return DataSet(ids, X, y)


def import_signal(data_proportion: float =1.0,
                  moving_average_window: Optional[int] = None,
                  should_scale: bool = True,
                  log_transform: bool = False,
                  clip_values: Optional[Tuple[float, float]] = None,
                  perform_fft: bool = False,
                  should_vectorise: bool = True,
                  training_ids: Optional[np.array] = None,
                  remove_incomplete_sequences: bool = True,
                  de_noise: bool = False) -> \
        Tuple[DataSet, DataSet, Optional[DataSet], Optional[DataSet]]:
    """Import the signal data-set from file_path
    
    Keyword Arguments:
        data_propotion {float} -- Proportion of data to return for training
            (default: {1.0})
        moving_average_window {Optional[int]} -- Window to perform a moving
            average on (default: {None})
        should_scale {bool} -- Whether or not to normalise inputs
            (default: {True})
        log_transform {bool} -- Whether or not to apply log transform to inputs
            (default: {False})
        clip_values {Optional[Tuple[Float, Float]]} -- Values to clip the X 
            values at. (default: {None})
        perform_fft {bool} -- Whether or not to perform a fast fourier transform
            on the data. (default: {False})
        should_vectorise {bool} -- Whether or not to vectorise the y categories.
            on the data. (default: {True})
        training_ids {Optional[np.array]} -- IDs which define the test/train 
            split.           
        remove_incomplete_sequences {bool} -- Remove sequences which don't have
            the full 6000 data points.
        de_noise {bool} -- Whether to de-noise the signal using FFT and inverse.
    Returns:
        Tuple[DataSet, DataSet, Optional[DataSet], Optional[DataSet]] -- 
            (training_data, testing_data, training_data_fft, testing_data_fft)
    """
    data = _get_raw_data()
    data = data[data.Type != '~']

    gc.collect()

    if data_proportion < 1.0:
        # Throw away some of the data so we can train a bit faster.
        data = data.sample(frac=data_proportion)

    ids, X, y = split_features_and_outcomes(data)

    gc.collect()

    if should_vectorise:
        y = vectorise(y)

    X = np.array(list([[i for i in x if np.isfinite(i) 
                                     and isinstance(i, float)] for x in X]))


    gc.collect()

    # Throw away data which has too little information.
    # Retain enough information for 300 frequency datapoints 
    # (nyquist => 2*300 = 600 time-points retained as a minimum)
    Xs_and_ys = [(i, x, y) for (i, x, y) in zip(ids, X, y) if len(x) >= 600]
    ids = np.array(list([i for (i, x, y) in Xs_and_ys]))
    X = np.array(list([x for (i, x, y) in Xs_and_ys]))
    y = np.array(list([y for (i, x, y) in Xs_and_ys]))

    gc.collect()

    if should_scale:
        X = np.array(list([scale(x) for x in X]))

    if moving_average_window is not None:
        X = np.array(list([get_moving_average(x, moving_average_window) for x in X]))

    if log_transform:
        X = np.add(X, -(np.min(X)-1))
        X = np.log(X)
    
    if clip_values is not None:
        X = np.array([np.clip(x, clip_values[0], clip_values[1]) for x in X])

    if remove_incomplete_sequences:
        Xs_and_ys = [(i, x, y) for (i, x, y) in zip(ids, X, y) 
                        if len(x) == 6000]
        ids = np.array(list([i for (i, x, y) in Xs_and_ys]))
        X = np.array(list([x for (i, x, y) in Xs_and_ys]))
        y = np.array(list([y for (i, x, y) in Xs_and_ys]))


    # Use rand seed to ensure same train/test split from train_test_split
    rand_seed = np.random.randint(low=0, high=2**32 - 1, size=1)[0]

    if perform_fft:
        X_fft = ([np.abs(np.fft.rfft(x)) for x in X]) 
        # Only take the first 300 components of the Frequency data
        # this seems to contain most of the information.
        X_fft = np.array([np.array(x[0:300]) for x in X_fft])
        ids_train, ids_test, X_train, X_test, y_train, y_test = \
            split_training_and_testing_data(ids, X_fft, y, 
                                            random_state=rand_seed, 
                                            training_ids=training_ids)
        training_data_fft = DataSet(ids_train, X_train, y_train)
        testing_data_fft = DataSet(ids_test, X_test, y_test)
    else:
        if de_noise:
            # Let's de-noise the signal a bit.
            X_fft = [np.fft.rfft(x) for x in X]
            # Only take the first 300 components of the Frequency data
            # this seems to contain most of the information.
            X_fft = np.array([np.array(x[0:300]) for x in X_fft])

            X = [np.fft.irfft(x) for x in X_fft]

        X = np.array([np.array([np.array([xi]) for xi in x]) for x in X])


        training_data_fft = None
        testing_data_fft = None

    gc.collect()

    ids_train, ids_test, X_train, X_test, y_train, y_test = \
        split_training_and_testing_data(ids, X, y, random_state=rand_seed, 
                                        training_ids=training_ids)

    training_data = DataSet(ids_train, X_train, y_train)
    testing_data = DataSet(ids_test, X_test, y_test)    

    gc.collect()

    return training_data, testing_data, training_data_fft, testing_data_fft


def get_moving_average(values: Iterable[float], window: int) -> \
        Iterable[Optional[float]]:
    """Performs a moving average on the values input with a given window size.
    
    Arguments:
        values {Iterable[float]} -- Values to perform moving average on.
        window {int} -- Window size.
    
    Returns:
        Iterable[Optional[float]] -- Moving average.
    """
    values_in_window: List[float] = []

    for current_value in values:
        num_values_in_window = len(values_in_window)
        if num_values_in_window < (window - 1):
            values_in_window.append(current_value)
        else:
            # need to re-jig values_in_window
            if num_values_in_window == window:
                values_in_window.pop(0)

            values_in_window.append(current_value)
            # Need to return moving average
            yield np.mean(values_in_window)


def plot_data(x: np.array, file_out: str):
    """Allows plotting of data from this import.
    
    Arguments:
        x {np.array} -- Data-set to plot. Should be invidiaul ECG record.
        file_out {str} -- where to save the file.
    """

    ax: pyplot.Axes
    fig: pyplot.Figure
    fig, ax = pyplot.subplots(figsize=(10, 7))

    len_x = len(x)
    x.reshape(len_x)

    ax.yaxis.set_label_text("Signal")
    ax.xaxis.set_label_text("Time Steps /0.0033 seconds")

    ax.plot(range(0, len_x), x)

    fig.savefig(file_out)

def get_fft_pca(n_components: int = 10,
                data_proportion: float = 1.0,
                should_vectorise: bool = False,
                training_ids: Optional[np.array] = None) -> \
        Tuple[DataSet, DataSet, Optional[DataSet], Optional[DataSet]]:
    """Get FFT data with PCA applied.
    
    Keyword Arguments:
        n_components {int} -- number of PCA components (default: {10})
        data_proportion {float} -- proportion of data to read (default: {1.0})
        should_vectorise {bool} -- proportion of data to read (default: {False})
        training_ids {Optional[np.array]} -- IDs which define the test/train 
            split.           

    Returns:
        Tuple[DataSet, DataSet, Optional[DataSet], Optional[DataSet]]: -- 
            (training_data, testing_data, training_data_fft, testing_data_fft)
    """
    training_data, testing_data, training_data_fft, testing_data_fft = \
        import_signal(should_scale=False, 
                      perform_fft=True, 
                      should_vectorise=should_vectorise,    
                      data_proportion=data_proportion,
                      training_ids=training_ids,
                      remove_incomplete_sequences=True)

    if n_components < 300:
        pca: PCA = PCA(n_components=n_components, whiten=True)

        pca.fit(training_data_fft.X)

        dt_str = str(datetime.now().timestamp())
        out_file_path: str = os.path.join("models", f"signal_pca_{dt_str}.sklearn")

        training_data_fft.X = pca.transform(training_data_fft.X)
        testing_data_fft.X = pca.transform(testing_data_fft.X)
        
        with open(out_file_path, 'wb+') as f:
            pickle.dump(pca, f)

    return training_data, testing_data, training_data_fft, testing_data_fft