"""Module for importing features data.

Occurrence of individual data-types:

>>> data.groupby(["Type"])["Type"].count()
Type
A    1160
N    7721
O    3857
~     324

"""
from typing import Iterable, List, Dict, Tuple, Optional
import pandas as pd
from pandas import DataFrame
from sklearn.model_selection import train_test_split
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale
import numpy as np
from keras.utils import to_categorical

from models.DataSet import DataSet, map_category_to_number, map_vector_to_category
import cache

_features_train_path = "~/data/csm6420/train_feat.csv"
_features_test_path = "~/data/csm6420/test_feat.csv"

n_features = 188
n_classes = 3 # 'A', 'N', 'O'

def _get_raw_data() -> DataFrame: 
    """Get raw data from file. Hold in memory as a cache.
    
    Returns:
        DataFrame -- Raw imported data.
    """
    raw_data = cache.get_dataframe("features_data")
    if raw_data is None:
        raw_data = pd.read_csv(_features_train_path)
        cache.save_dataframe_in_cache("features_data", raw_data)

    return raw_data
def get_ids_for_stratified_split() -> (np.array, np.array):
    """Return a list of all the raw ids in the Signal CSV file.
    
    Returns:
        (np.array, np.array) -- (ids, ys) for stratified split.
    """

    data = _get_raw_data()
    return (np.array(data["ID"]), np.array(data["Type"]))

def split_features_and_outcomes(data: DataFrame,
                                scale_columns: bool = False) -> \
        Tuple[np.array, np.array, np.array]:
    """Split apart the features from the outcomes.
    
    Arguments:
        data {DataFrame} -- Data to split.
        scale_columns {bool} -- Whether or not to scale the 
            columns to a unit normal distribution. {default:False}
    
    Returns:
        Tuple[np.array, DataFrame, np.array] -- 
            tuple of (ids, features, outcomes) i.e. (ids, X, y)
            Outcomes will be empty string values if performed on 
            validation dataset.
    """
    column_names = data.keys()
    feature_columns = [c for c in column_names if c != "Type" and c != "ID"]

    ids: np.array = np.array(data["ID"])

    if scale_columns:
        X: np.array = DataFrame(
            [scale(np.array(data[col])) for col in feature_columns]
        ).transpose().to_numpy()
    else:
        X: np.array = data[feature_columns].to_numpy()

    if "Type" in data.keys():
        y: np.array = np.array(data["Type"])
    else:
        # Code path used when importing from validation data files where true
        # classification is not known.
        y: np.array = np.array([None] * len(ids))

    return (ids, X, y)


def _remove_non_finite_numbers(df: DataFrame) -> None:
    """Replace non-finite numbers with 0.0 for any given feature.
    
    Arguments:
        df {DataFrame} -- Input
    """
    cols_to_scan = [k for k in df.keys() if k != "ID" and k != "Type"]
    for i, row in df.iterrows():
        for column in cols_to_scan:
            val = row[column]
            if not np.isfinite(val):
                print(f"Setting {val} to 0.0 in {i}.{column}")
                df.ix[i, column] = 0.0

    # for col in cols_to_scan:
    #     df[col] = scale(df[col])

def vectorise(y: np.array) -> np.array:
    """Vectorise the y array.
    
    Arguments:
        y {np.array} -- Array of classification values to vectorise.
    
    Returns:
        np.array -- Vectorised classification values.
    """
    y = np.array([map_category_to_number(y_i) for y_i in y])
    y = to_categorical(y)
    return np.array(list(y))

def split_training_and_testing_data(ids: np.array, X: np.array, y: np.array, \
                                    training_ids: Optional[np.array] = None,
                                    random_state: Optional[int] = None) \
                                        -> Tuple[np.array, np.array, np.array,
                                                 np.array, np.array, np.array]:
    """Split training and test datasets
    
    Arguments:
        ids {np.array} -- ids
        X {np.array} -- X features
        y {np.array} -- y classes
    
    Keyword Arguments:
        training_ids {Optional[np.array]} -- 
            IDs of records which are in the training data-set (default: {None})
        random_state {Optional[np.array]} -- 
            Used to ensure train_test_split is predictable (default: {None})
    
    Returns:
        Tuple[np.array, np.array, np.array, np.array, np.array, np.array] -- 
            (ids_train, ids_test, X_train, X_test, y_train, y_test)
    """
    if training_ids is None:
        return train_test_split(ids, X, y, test_size=0.2, shuffle=True, 
                                stratify=y, random_state=random_state)
    else:
        training_ids = set(training_ids)

        train_ind = list([i for i, x in enumerate(ids) if x in training_ids])
        test_ind = list([i for i, x in enumerate(ids) if x not in training_ids])

        ids_train = np.array(list(ids[train_ind]))
        ids_test = np.array(list(ids[test_ind]))

        X_train = np.array(list(X[train_ind]))
        X_test = np.array(list(X[test_ind]))

        y_train = np.array(list(y[train_ind]))
        y_test = np.array(list(y[test_ind]))

        return ids_train, ids_test, X_train, X_test, y_train, y_test

def import_features_validation() -> DataSet:
    """Function to import features from a file.
    
    Returns:
        DataSet -- Data-set containing validation data.
    """
    data = pd.read_csv(_features_test_path, dtype={"ID": "str"})    

    _remove_non_finite_numbers(data)

    ids, X, y = split_features_and_outcomes(data, scale_columns=True)
  
    return DataSet(ids, X, y)

def import_features(should_vectorise: bool = True,
                    training_ids: Optional[np.array] = None) ->\
        Tuple[DataSet, DataSet]:
    """Function to import features from a file.
    
    Arguments:
        should_vectorise {bool} -- 
            Whether the results should be vectorised or not.
        training_ids {Optional[np.array]} -- IDs which define the test/train 
            split.           

    Returns:
        Tuple[DataSet, DataSet] -- 
            Tuple of (training_data, testing_data).
    """
    
    # Remove data from here with unknown y values, i.e. the "~" value.
    data = _get_raw_data()
    data = data[data.Type != "~"]

    _remove_non_finite_numbers(data)

    ids, X, y = split_features_and_outcomes(data, scale_columns=True)
  

    if should_vectorise:
        y = vectorise(y)

    ids_train, ids_test, X_train, X_test, y_train, y_test = \
        split_training_and_testing_data(ids, X, y, training_ids=training_ids)

    return DataSet(ids_train, X_train, y_train), \
            DataSet(ids_test, X_test, y_test)


def get_pca_transformed_data(file_path: str = _features_train_path, 
                             n_components: int = 10,
                             vectorise: bool = True,
                             training_ids: Optional[np.array] = None) -> \
        Tuple[DataSet, DataSet]:
    """Import Features data from the file_path, split it into a test and 
    training data-set and then perform PCA on the X components.
    
    Keyword Arguments:
        file_path {str} -- file to import from 
            (default: {_features_train_path})
        n_components {int} -- number of PCA components to include
            (default: {10})
        vectorise {bool} -- Whether the results should be vectorised or not.
        training_ids {Optional[np.array]} -- IDs which define the test/train 
            split.           
    
    Returns:
        Tuple[DataFrame, DataFrame, np.array, np.array] -- 
            (training_data, testing_data) where the training data has been 
            PCA transformed.
    """
    pca: PCA = PCA(n_components, whiten=True)

    training_data, testing_data = import_features(vectorise, training_ids)

    pca.fit(training_data.X)    

    training_data.X = pca.transform(training_data.X)
    testing_data.X = pca.transform(testing_data.X)

    return training_data, testing_data
