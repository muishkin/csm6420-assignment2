"""Module holding Data-Set class for holding ids, X and y data-sets."""
import numpy as np
from typing import Tuple, Dict, List, Any
from pandas import DataFrame, Series, concat

def map_category_to_number(category: str) -> int:
    """Will map a text category to a numeric value.
    
    Arguments:
        category {str} -- [description]
    
    Raises:
        ValueError -- [description]
    
    Returns:
        int -- [description]
    """

    if category == 'A':
        return 0
    if category == 'N':
        return 1
    if category == 'O':
        return 2
    if category == '~':
        return 3

    raise ValueError(f"No matching category found for {category}")

def map_vector_to_category(prediction_vector: np.array) -> str:
    """Will map a numeric value to a category char.
    
    Arguments:
        prediction_vector {np.array} - Probabilities of each class
    
    Raises:
        ValueError
    
    Returns:
        char
    """
    category = np.where(prediction_vector == np.max(prediction_vector))[0][0]

    if category == 0:
        return 'A'
    if category == 1:
        return 'N'
    if category == 2:
        return 'O'
    if category == 3:
        return '~'

    raise ValueError(f"No matching category found for {str(category)}")


class DataSet(object):
    """Class for holding ids, X and y data-sets."""

    ids: np.array
    X: np.array
    y: np.array


    def __init__(self, ids: np.array, X: np.array, y: np.array):
        self.ids = ids
        self.X = X
        self.y = y


    def to_dataframe(self) -> DataFrame:
        """Return dataframe representing this data-set.
        
        Returns:
            DataFrame --
        """
        df = DataFrame.from_records(self.X)
        df["ID"] = self.ids
        df["y"] = list(self.y)

        df.set_index("ID")

        return df

    @staticmethod
    def from_dataframe(df: DataFrame):
        """Generate a DataSet from a data-fram.
        
        Arguments:
            df {DataFrame} -- DataFrame to generate from.
        """

        other_cols = [k for k in df.keys() if k != "ID" and k != "y"]
        
        ids = np.array(df["ID"])

        features = df[other_cols].to_numpy()
 
        ys = np.array([np.array(y) for y in df["y"]])

        return DataSet(ids, features, ys)

    def join(self, other):
        """Joins this data-set with another data-set. Performs a full outer 
        join leaving empty records for each where they cannot be found.
        
        Arguments:
            other {DataSet} -- Other data-set to join.
        
        Returns:
            Tuple[DataSet, DataSet] -- 
                Joined data-sets tuple: (this, other)
        """
        df1: DataFrame = self.to_dataframe()
        df2: DataFrame = other.to_dataframe()

        left_keys_orig = df1.keys()
        right_keys_orig = df2.keys()

        df1 = df1.rename(columns= 
            dict([(k, f"{k}_left") for k in left_keys_orig])
        )

        df2 = df2.rename(columns= 
            dict([(k, f"{k}_right") for k in right_keys_orig])
        )

        df1.set_index(df1.ID_left)
        df2.set_index(df2.ID_right)

        df = df1.join(df2, how='outer', sort=False)

        left_keys = [k for k in df.keys() if k.endswith('_left')]
        right_keys = [k for k in df.keys() if k.endswith('_right')]

        df_left = df[left_keys]
        df_right = df[right_keys]

        df_left = df_left.rename(columns= 
            dict([(f"{k}_left", k) for k in left_keys_orig])
        )

        df_right = df_right.rename(columns= 
            dict([(f"{k}_right", k) for k in right_keys_orig])
        )

        ds_left = DataSet.from_dataframe(df_left)
        ds_right = DataSet.from_dataframe(df_right)

        return (ds_left, ds_right)

    def randomise(self):
        """Randomises the order of this data-set"""
        all_data = list(zip(self.ids, self.X, self.y))
        np.random.shuffle(all_data)

        self.ids = np.array([i for (i, x, y) in all_data])
        self.X = np.array([x for (i, x, y) in all_data])
        self.y = np.array([y for (i, x, y) in all_data])


    def balance_categories(self):
        """Balances the number of categories in this DataSet."""
        print("Balancing Categories")

        additional_weighting = {
            "A": 1, 
            "N": 1, 
            "O": 1, 
            "~": 1
        }

        categories: np.array = self.y
        map_category_to_inds: Dict[str, List[int]] = {}

        if len(categories.shape) > 1:
            categories = [map_vector_to_category(c) for c in categories]

        for i, cat in enumerate(categories):

            inds = map_category_to_inds.get(cat)

            if inds is None:
                inds = []
                map_category_to_inds[cat] = inds
            
            inds.append(i)

        num_required_for_balance = max([len(inds) for (cat, inds) 
                                        in map_category_to_inds.items()])

        self.ids = np.array(self.ids)
        self.X = np.array(self.X)
        self.y = np.array(self.y)


        for (cat, inds) in map_category_to_inds.items():
            num_required = num_required_for_balance * additional_weighting[cat]

            inds = np.array(inds)
            num_observed = len(inds)
            if num_observed == num_required:
                continue
            
            num_required = num_required_for_balance - num_observed

            indicies_to_dupe = inds[list(np.random.choice(inds.shape[0], 
                                                          num_required, 
                                                          replace=True))]

            self.ids = np.concatenate([self.ids, self.ids[indicies_to_dupe]])
            self.X = np.concatenate([self.X, self.X[indicies_to_dupe]])
            self.y = np.concatenate([self.y, self.y[indicies_to_dupe]])

        self.ids = np.array(self.ids)
        self.X = np.array(self.X)
        self.y = np.array(self.y)

        self.randomise()
        print("Finished Balancing Categories")

