"""Module containing code for importing test data sets and making predictions
from them."""
from keras.models import model_from_json, Model
from sklearn.ensemble import RandomForestClassifier
import pickle
import os
from pandas import DataFrame
from datetime import datetime

from experiments.features_signal_1 import predict
import data_import.signal as signal
import data_import.features as features


def _get_signal_classifier(dtstamp: str) -> Model:
    """Takes a JSON file and returns the corresponding Keras model. Assumes it
    is the LSTM model from `experiments/signal_2.py`.
    
    Arguments:
        dtstamp {str} -- DtStamp identifying the model.
    
    Returns:
        Model -- Trained Keras Model
    """
    file_path = os.path.join("models", f"signal_6_{dtstamp}.json")
    weights_file_path = os.path.join("models", 
                                     f"signal_6_weights_{dtstamp}.json")

    with open(file_path, 'r') as f:
        json = f.read()
        model: Model = model_from_json(json)
        model.load_weights(weights_file_path)

        model.compile(optimizer='RMSprop', loss='categorical_crossentropy', 
                      metrics=['accuracy'])

        return model

def _get_features_classifier(dtstamp: str) -> RandomForestClassifier:
    """Loads an sklearn features model from disk.
    
    Arguments:
        dtstamp {str} -- DtStamp identifying the model.
    
    Returns:
        RandomForestClassifier -- Trained RandomForestClassifier.
    """
    file_path = os.path.join("models", f"features_2_{dtstamp}.sklearn")
    with open(file_path, 'rb') as f:
        model: RandomForestClassifier = pickle.load(f)
        return model


def export_predictions(signal_dtstamp: str, features_dtstamp: str):
    """Exports predictions to CSV file.

    Find models with dtstamp values in `./models` directory.
    
    Arguments:
        signal_dtstamp {str} -- DtStamp of Signal model to use.
        features_dtstamp {str} -- DtStamp of Features model to use.
    """
    signal_data = \
        signal.import_signal_validation(clip_values=(-2, +2),
                                        de_noise=True)

    features_data = features.import_features_validation()

    signal_classifier: Model = _get_signal_classifier(signal_dtstamp)
    features_classifier: RandomForestClassifier = \
        _get_features_classifier(features_dtstamp)

    ids, y_true, y_pred = predict(signal_data, features_data, 
                                  signal_classifier, features_classifier, 
                                  features_weight=1.0, signal_weight=0.0)

    data_out = DataFrame({
        "ID": ids,
        "Predicted": y_pred
    })

    dt_str:str = str(datetime.now().timestamp())
    data_out.to_csv(f"predictions_{dt_str}.csv", index=False)

    