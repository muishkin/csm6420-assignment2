"""

Signal Experiment 4.

FFT approach combined with Random forest. F1 Score = 0.27830790868765554
Attempting with AdaBoost now F1 Score = 0.3500570087189805

"""

from sklearn.metrics import f1_score
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.metrics import f1_score, accuracy_score, make_scorer
import numpy as np

import data_import.features as features
from data_import.signal import import_signal, plot_data, fft_n_components
import cache
from models.DataSet import DataSet

def main():
    training_data = cache.get_dataframe("signal_4_training_data")
    testing_data = cache.get_dataframe("signal_4_testing_data")

    if training_data is None or testing_data is None:
        z, zz, training_data, testing_data = \
            import_signal(should_scale=False, perform_fft=True,
                          should_vectorise=False)
        cache.save_dataframe_in_cache("signal_4_training_data",             
                                    training_data.to_dataframe())
        cache.save_dataframe_in_cache("signal_4_testing_data",             
                                    testing_data.to_dataframe())
    else:
        training_data = DataSet.from_dataframe(training_data)
        testing_data = DataSet.from_dataframe(testing_data)



    print("Data-set imported... now training classifier(s).")

    param_grid = {
        "n_estimators": [50, 100, 200],
        "max_depth": [25, 30]
    }
    
    clf: GridSearchCV = GridSearchCV(RandomForestClassifier(), param_grid, 
                                     n_jobs=3, cv=3,
                                     scoring='accuracy')

    clf.fit(training_data.X, training_data.y)

    best_params = clf.best_params_
    best_score = clf.best_score_
    print(best_params)
    print(f"Gives score: {str(best_score)}")

    y_true = testing_data.y
    y_pred = clf.predict(testing_data.X)

    f_score = f1_score(y_true, y_pred, average='macro')
    print(f"F1 score = {str(f_score)}")



    # scores = clf.score(X_test, y_test)
    # print(scores)

    # y_pred = clf.predict(X_test)
    # f1 = f1_score(y_test, y_pred, average='macro')
    # print(f"F1 Score = {str(f1)}")

if __name__ == "__main__":
    main()


