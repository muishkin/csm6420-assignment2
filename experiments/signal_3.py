"""

Signal Experiment 3.

FFT approach

"""

from keras.models import Sequential, Model
from keras.layers import Input, Dense, Dropout
from sklearn.metrics import f1_score
import numpy as np
from datetime import datetime
import os

import data_import.features as features
from data_import.signal import get_fft_pca, plot_data, fft_n_components, import_signal
import cache
from models.DataSet import DataSet
from experiments.signal_2 import get_f1_score

_n_components = 100

def get_trained_model(training_data: DataSet) -> Model:
    """Takes a training dataset and created a fitted LSTM network designed
    to predict heart beat classification on arbitrary-length raw sequences.
    
    Arguments:
        training_data {DataSet} -- Data-set to train on.
    
    Returns:
        Model -- Trained model.
    """
    in_1 = Input(shape=(300,))
    path_1 = Dense(500, activation='relu')(in_1)
    path_1 = Dropout(0.6)(path_1)
    path_1 = Dense(50, activation='relu')(path_1)
    path_1 = Dense(10, activation='relu')(path_1)
    path_1 = Dense(features.n_classes, activation='softmax')(path_1)

    model = Model([in_1], path_1)

    model.compile(optimizer='Nadam', loss='categorical_crossentropy', 
                  metrics=['accuracy'])

    model.fit(training_data.X, training_data.y, epochs=15)

    model_json = model.to_json()
    dt_str: str = str(datetime.now().timestamp())
    out_file_path: str = os.path.join("models", f"signal_3_{dt_str}.json")
    out_weights_file_path: str = \
        os.path.join("models", f"signal_3_weights_{dt_str}.json")

    with open(out_file_path, 'w+') as f:
        f.write(model_json)
    model.save_weights(out_weights_file_path)

    return model

def main():
    z, zz, training_data, testing_data = import_signal(
        remove_incomplete_sequences=True,
        perform_fft=True,
        should_vectorise=True,
        should_scale=False
    )

    training_data.balance_categories()

    model = get_trained_model(training_data)
    scoring = model.evaluate(testing_data.X, testing_data.y)
    print(scoring)

    y_true = [features.map_vector_to_category(vec) for vec in testing_data.y]
    y_pred = [features.map_vector_to_category(vec) 
              for vec in model.predict(testing_data.X)]

    f_score = f1_score(y_true, y_pred, average='macro')
    print(f"F1 score = {str(f_score)}")



