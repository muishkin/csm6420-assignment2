"""

Features + Signal Experiment 1.

Combination of Decision Forest(features) and Basic NN of FFT(signal).

Signal classifier has an F1 score of 0.2515477354187032
Features classifier has an F1 score of 0.8250534633821146
F1 Score = 0.8255860004479106

"""

from keras.models import Model
from sklearn.metrics import f1_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
import numpy as np
from pandas import DataFrame
from typing import List, Tuple

import data_import.features as features
import data_import.signal as signal
from models.DataSet import DataSet, map_vector_to_category
import experiments.signal_2 as signal_lstm_experiment
import experiments.signal_3 as signal_fft_experiment
import experiments.features_2 as features_random_forest_experiment


def _get_training_ids_for_all_records() -> np.array:
    """Return training ids for testing which take in to consideration all of 
    the test cases in both Signals and Features CSV files.

    IDs end up shuffled and stratified according to output classification.
    
    Returns:
        np.array -- IDs to train on. The remaining IDs represent test cases.
    """
    signal_ids, signal_ys = signal.get_ids_for_stratified_split()
    feature_ids, features_ys = features.get_ids_for_stratified_split()

    unique_ids_set = set()

    concat_ids = list(signal_ids) + list(feature_ids)
    concat_ys = list(signal_ys) + list(features_ys)

    unique_ids = []
    ys = []

    for (i, y) in zip(concat_ids, concat_ys):
        if not i in unique_ids:
            unique_ids_set.add(i)
            unique_ids.append(i)
            ys.append(y)

    training_ids, testing_ids  = train_test_split(unique_ids, test_size=0.2, 
                                                  shuffle=True, stratify=ys)

    return training_ids

def remove_untestable_data(dataset: DataSet, is_vectorised: bool = False) -> DataSet:
    """Takes a data-set and strips out all of the records which cannot be
    tested.
    
    Arguments:
        dataset {DataSet} -- 
    
    Keyword Arguments:
        is_vectorised {bool} -- 
            Whether or not the y vector is vectorised (default: {False})
    
    Returns:
        DataSet -- 
    """
    indicies = [i for i, x in enumerate(dataset.ids) if isinstance(x, str)]

    ids = np.array(dataset.ids[indicies])
    xs = np.array(dataset.X[indicies])
    ys = np.array(dataset.y[indicies])

    if is_vectorised:
        ys = np.array([map_vector_to_category(vec) for vec in ys])

    return DataSet(ids, xs, ys)

def main():
    # n_pca_components = 50

    training_ids = _get_training_ids_for_all_records()

    z, zz, s_training_data, s_testing_data = \
        signal.get_fft_pca(n_components=signal_fft_experiment._n_components,
                           should_vectorise=True,
                           training_ids=training_ids)

    s_training_data.balance_categories()

    f_training_data, f_testing_data = \
        features.import_features(should_vectorise=False, 
                                 training_ids = training_ids)

    f_training_data.balance_categories()

    signal_classifier: Model = \
        signal_fft_experiment.get_trained_model(s_training_data)

    features_classifier: RandomForestClassifier = \
        features_random_forest_experiment.get_trained_model(f_training_data)

    # Calculate F1 scores for individual classifiers.
    signal_testing_data = remove_untestable_data(s_testing_data)
    f_score_signal_classifier = \
        signal_fft_experiment.get_f1_score(signal_classifier, 
                                           signal_testing_data)   

    print("Signal classifier has an F1 score of " + 
          str(f_score_signal_classifier))

    features_testing_data = remove_untestable_data(f_testing_data)
    f_score_features_classifier = \
        features_random_forest_experiment.get_f1_score(features_classifier, 
                                                       features_testing_data)

    print("Features classifier has an F1 score of " + 
          str(f_score_features_classifier))


    # Now to join together the records so that we can classify using both 
    # classifiers together.
    ids, y_true, y_pred = predict(s_testing_data, f_testing_data, signal_classifier, 
                                  features_classifier, f_score_features_classifier, 
                                  f_score_signal_classifier)

    f1 = f1_score(y_true, y_pred, average='macro')
    print(f"F1 Score = {str(f1)}")

def predict(signal_data: DataSet, features_data: DataSet, 
            signal_classifier: Model, 
            features_classifier: RandomForestClassifier, 
            features_weight: float, 
            signal_weight: float,
            is_validation_run: bool = False) -> Tuple[np.array, np.array]:
    """Predict using both Features and Signal data-set. Using appropriate 
    weighting from individual classifier f_scores.
    
    Arguments:
        signal_data {DataSet, features_data} -- Signal data-set.
        features_data {DataSet} -- Features data-set
        signal_classifier {Model} -- Signal Classifier (LSTM)
        features_classifier {RandomForestClassifier} -- Features classifier.
        features_weight {float} -- 
            How to weight features classifier prediction results.
        signal_weight {float} -- 
            How to weight signal classifier prediction results.
        is_validation_run {bool} -- Whether or not we are running the 
            validation data-set. If true, then y_true is an array of `None`
            elements. {default: False}
    
    Returns:
        Tuple[np.array, np.array, np.array] -- (ids, y_true, y_predict)
    """

    # Now to join together the records so that we can classify using both 
    # classifiers together.
    signal_data, features_data = signal_data.join(features_data)

    s_only_prediction_ind: List[int] = []
    f_only_prediction_ind: List[int] = []
    joint_prediction_ind: List[int] = []

    all_ids = list(zip(signal_data.ids, features_data.ids))
    for i, (signal_id, feature_id) in enumerate(all_ids):
        if type(feature_id) != str:
            s_only_prediction_ind.append(i)
        elif type(signal_id) != str:
            f_only_prediction_ind.append(i)
        else:
            joint_prediction_ind.append(i)

    y_pred = []
    y_true = []
    # Now make the predictions using the best available predictor or 
    # combination of predictors.
    if any(s_only_prediction_ind):
        signal_feat = np.array(signal_data.X[s_only_prediction_ind])
        signal_feat = signal_feat.reshape(signal_feat.shape[0], 
                                          signal_feat.shape[1], 1)

        predictions = signal_classifier.predict(signal_feat)

        # y_pred += list(predictions)
        y_pred += [map_vector_to_category(p) for p in predictions]

        if is_validation_run:
            y_true += [None for i in signal_data.ids]
        else:
            y_true += [map_vector_to_category(y) 
                        for y in signal_data.y[s_only_prediction_ind]]


    if any(f_only_prediction_ind):
        feat_features = np.array(features_data.X[f_only_prediction_ind])
        y_pred += list(features_classifier.predict(feat_features))
        if is_validation_run:
            y_true += [None for i in features_data.ids]
        else:
            y_true += list(features_data.y[f_only_prediction_ind])

    if any(joint_prediction_ind):
        # Can use both the signals and the features predictors here.
        # Maybe we can do some weighting to see whether we're 
        # For now just use features here as it's the superior predictor:
        feature_feat = np.array(features_data.X[joint_prediction_ind])
        signal_feat = np.array(signal_data.X[joint_prediction_ind])
        signal_feat = signal_feat.reshape(signal_feat.shape[0], 
                                          signal_feat.shape[1], 1)

        feat_probs = features_classifier.predict_proba(feature_feat)
        sig_probs = signal_classifier.predict(signal_feat)

        feat_probs = np.array(feat_probs)
        sig_probs = np.array(sig_probs)

        # Weight predictions according to previous F1 scores.
        avg_probs = np.add(
            np.multiply(feat_probs, features_weight),
            np.multiply(sig_probs, signal_weight)
        )

        if is_validation_run:
            y_true += [None for i in features_data.ids]
        else:
            y_true += list(features_data.y[joint_prediction_ind])

        y_pred += [features.map_vector_to_category(p) for p in avg_probs]

    y_true = np.array(y_true)

    indicies_in_order = list(s_only_prediction_ind + f_only_prediction_ind +
        joint_prediction_ind)

    all_ids = np.array([li if isinstance(li, str) else ri 
                        for (li, ri) in all_ids])

    ids = np.array(all_ids[indicies_in_order])

    return ids, y_true, y_pred

if __name__ == "__main__":
    main()
