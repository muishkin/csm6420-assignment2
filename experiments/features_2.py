"""

Experiment 2

Attempting to generate a simple random forest classifier for the features 
data-set.

F1 Score = 0.813233648727466
0.8275899230321913 with category balancing.

F1 scores for ordered categories:
0.8221709006928405

0.8894536213468869

0.7392739273927393

0.5846153846153846
"""
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import f1_score, accuracy_score, make_scorer
import pickle
import os
from datetime import datetime
import numpy as np

from data_import.features import import_features
from models.DataSet import DataSet

def get_trained_model(dataset: DataSet) -> RandomForestClassifier:
    """Get a trained random forest classifier for the given data-set-.    
    Arguments:
        dataset {DataSet} -- Data-set to train classifier on.
    
    Returns:
        RandomForestClassifier -- Trained classifier.
    """
    # param_grid = {
    #     "n_estimators": [250, 300, 350],
    #     "max_depth": [150, 200, 250]
    # }

    # clf: GridSearchCV = GridSearchCV(RandomForestClassifier(), param_grid, 
    #                                  n_jobs=3, cv=3,
    #                                  scoring='accuracy')

    # clf.fit(dataset.X, dataset.y)

    # best_params = clf.best_params_
    # best_score = clf.best_score_
    # print(best_params)
    # print(f"Gives score: {str(best_score)}")

    clf: RandomForestClassifier = RandomForestClassifier(n_estimators=300, 
                                                         max_depth=200)
    clf.fit(dataset.X, dataset.y)

    # Save this model to disk!
    dt_str: str = str(datetime.now().timestamp())
    out_file_path: str = os.path.join("models", f"features_2_{dt_str}.sklearn")
    with open(out_file_path, 'wb+') as f:
        pickle.dump(clf, f)

    return clf


def get_f1_score(model: RandomForestClassifier, data: DataSet) -> float:
    """Get the F1 score for the given Random Forest Classifier.
    
    Arguments:
        model {RandomForestClassifier} -- Model to get F1 score for.
        data {DataSet} -- Data for testing.
    
    Returns:
        float -- F1 score.
    """
    y_pred = model.predict(data.X)

    for true_cat in ['A', 'N', 'O', '~']:   
        cy_true = np.array(data.y == true_cat)
        cy_pred = np.array(y_pred == true_cat)
        f1 = f1_score(cy_true, cy_pred, average='binary')
        print(str(f1))

    return f1_score(data.y, y_pred, average='macro')


def main():
    training_data, testing_data = import_features(should_vectorise=False)

    training_data.balance_categories()

    clf = get_trained_model(training_data)

    scores = clf.score(testing_data.X, testing_data.y)
    print(scores)

    f1 = get_f1_score(clf, testing_data)
    print(f"F1 Score = {str(f1)}")

if __name__ == "__main__":
    main()
