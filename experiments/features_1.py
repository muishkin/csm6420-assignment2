"""
Experiment 1 for playing with features.

This experiment is a simple Neural Network ingesting the PCA transformed 
training data-set with one hidden layer with relu activation function.

F1 Score = 0.788158770993394

"""
from keras.models import Sequential
from keras.layers import Dense, Dropout
from sklearn.metrics import f1_score
import numpy as np

from data_import.features import get_pca_transformed_data, n_features, n_classes

def main():
    n_components = 50
    training_data, testing_data = \
        get_pca_transformed_data(n_components=n_components)

    model: Sequential = Sequential()
    model.add(Dense(units=40, activation='relu', input_dim=n_components))
    #model.add(Dropout(0.4))
    #model.add(Dense(units=20, activation='relu'))
    model.add(Dropout(0.4))
    model.add(Dense(units=10, activation='relu'))
    model.add(Dropout(0.2))
    model.add(Dense(units=n_classes, activation='softmax'))

    model.compile('Nadam', loss='categorical_crossentropy', 
                  metrics=['accuracy', 'categorical_accuracy'])

    model.fit(training_data.X, training_data.y, epochs=50, batch_size=20)

    print(model.evaluate(testing_data.X, testing_data.y))

    y_pred = model.predict(testing_data.X)

    y_pred = np.array([[float(p) for p in ps == np.max(ps)] for ps in y_pred])

    f1 = f1_score(testing_data.y, y_pred, average='macro')
    print(f"F1 Score = {str(f1)}")


if __name__ == "__main__":
    main()
