"""

Experiment 3

Attempting to generate a Naive Bayes classifier for the features data-set.

F1 Score = 0.6717857792927483

"""
from sklearn.model_selection import GridSearchCV
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import f1_score, accuracy_score, make_scorer
import numpy as np

from data_import.features import import_features

def main():
    training_data, testing_data = import_features(should_vectorise=False)

    clf: GaussianNB = GaussianNB()

    clf.fit(training_data.X, training_data.y)

    scores = clf.score(testing_data.X, testing_data.y)
    print(scores)

    y_pred = clf.predict(testing_data.X)
    f1 = f1_score(testing_data.y, y_pred, average='macro')
    print(f"F1 Score = {str(f1)}")

if __name__ == "__main__":
    main()
