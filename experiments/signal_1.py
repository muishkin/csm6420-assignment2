# """

# Signal Experiment 1.

# Attempt to generate a simple LSTM Neural Network to predict the classification
# of of a signal from a variable length time-series input.

# """

# from keras.models import Sequential, Model
# from keras.layers import CuDNNLSTM, Conv1D, Input, Dense, MaxPooling1D, LSTM, Concatenate
# from sklearn.metrics import f1_score
# import numpy as np

# from data_import.features import n_classes
# from data_import.signal import import_signal


# def get_batches_same_length(xs, ys):
#     map_len_to_X = {}
#     map_len_to_y = {}

#     for (x, y) in zip(xs, ys):
#         l = len(x)

#         if l not in map_len_to_X:
#             map_len_to_X[l] = []
#             map_len_to_y[l] = []

#         map_len_to_X[l].append(x)
#         map_len_to_y[l].append(y)

#     for l, X in map_len_toperform_fftyield (np.array(X), np.array(map_len_to_y[l]))

# training_data, testing_data = import_signal(data_proportion=0.02)

# # y_train_old = y_train
# # y_test_old = y_test
# # 0.9, 0.5-0.6, 0.7
# inputs = []
# early_paths = []
# paths = []

# for i in range(0, n_classes):
#     in_i = Input(shape=(None, 1))
#     path_i = LSTM(10)(in_i)
    
#     early_paths.append(path_i)

#     path_i = Dense(1, activation='sigmoid')(path_i)

#     inputs.append(in_i)
#     paths.append(path_i)

#     model_i = Model([in_i], path_i)
#     model_i.compile(optimizer='Nadam', loss='binary_crossentropy', metrics=['accuracy'])

#     v = [j == i for j in range(0, n_classes)]

#     for (X, y) in get_batches_same_length(X_train, y_train):
#         model_i.fit(X, np.dot(y, v), epochs=50)

# joined_path = Concatenate()(early_paths)
# joined_path = Dense(10, activation='sigmoid')(joined_path)
# joined_path = Dense(n_classes, activation='softmax')(joined_path)

# model = Model(inputs, joined_path)

# model.compile(optimizer='Nadam', loss='categorical_crossentropy', metrics=['accuracy'])

# for (X, y) in get_batches_same_length(X_train, y_train):
#     model.fit([X] * len(inputs), y, epochs=5, batch_size=20)


# for (X, y) in get_batches_same_length(X_test, y_test):
#     scoring = model.evaluate(X, y)
#     print(scoring)



