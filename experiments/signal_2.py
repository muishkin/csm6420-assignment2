"""

Signal Experiment 2.

Moving average attempt with LSTM.

"""

from keras.models import Sequential, Model
from keras.layers import CuDNNLSTM, Conv1D, Input, Dense, MaxPooling1D, LSTM, Concatenate
from sklearn.metrics import f1_score
import numpy as np
import gc
import cython
from typing import Iterable, Tuple, Dict, List
from datetime import datetime
import os

import data_import.features as features
from data_import.signal import import_signal, plot_data
import cache
from models.DataSet import DataSet


def get_batches_same_length(xs: np.array, ys: np.array) -> Iterable[Tuple[np.array, np.array]]:
    """Take data of varying length and split it in to batches with the same 
    length.
    
    Arguments:
        xs {np.array} -- X (features) input
        ys {np.array} -- y (classes) input
    
    Returns:
        Iterable[Tuple[np.array, np.array]] -- Batches grouped by same size.
    """
    map_len_to_X: Dict[int, List[np.array]] = {}
    map_len_to_y: Dict[int, List[np.array]] = {}

    for (x, y) in zip(xs, ys):
        l = len(x)

        if l not in map_len_to_X:
            map_len_to_X[l] = []
            map_len_to_y[l] = []

        map_len_to_X[l].append(x)
        map_len_to_y[l].append(y)

    for l, X in map_len_to_X.items():
          yield (np.array(X), np.array(map_len_to_y[l]))

def get_trained_model(training_data: DataSet) -> Model:
    """Takes a training dataset and created a fitted LSTM network designed
    to predict heart beat classification on arbitrary-length raw sequences.
    
    Arguments:
        training_data {DataSet} -- Data-set to train on.
    
    Returns:
        Model -- Trained model.
    """
    in_1 = Input(shape=(None, 1))
    path_1 = CuDNNLSTM(20)(in_1)
    path_1 = Dense(10, activation='sigmoid')(path_1)

    path_1 = Dense(features.n_classes, activation='softmax')(path_1)

    model = Model([in_1], path_1)

    model.compile(optimizer='RMSprop', loss='categorical_crossentropy', 
                  metrics=['accuracy'])

    # Need to fit the model with each input length seperately.
    for (X, y) in get_batches_same_length(training_data.X, training_data.y):
        model.fit(X, y, epochs=20)

    model_json = model.to_json()
    dt_str: str = str(datetime.now().timestamp())
    out_file_path: str = os.path.join("models", f"signal_2_{dt_str}.json")
    out_weights_file_path: str = \
        os.path.join("models", f"signal_2_weights_{dt_str}.json")

    with open(out_file_path, 'w+') as f:
        f.write(model_json)
    model.save_weights(out_weights_file_path)

    return model

def get_f1_score(model: Model, testing_data: DataSet, 
                 vector_data: bool = True) -> float:
    """Calculates the F1 score for a given Keras model given a data-set.
    
    Arguments:
        model {Model} -- Model to test.
        testing_data {DataSet} -- Data to test with.
    
    Keyword Arguments:
        vector_data {bool} -- 
            Whether the model uses vectorised categories (default: {True})
    
    Returns:
        float -- [description]
    """
    y_true = []
    y_pred = []
    for X, y in get_batches_same_length(testing_data.X, testing_data.y):
        y_true += list(y)
        y_pred += list(model.predict(X))

    if vector_data:
        y_pred = [features.map_vector_to_category(vec) for vec in y_pred]
        y_true = [features.map_vector_to_category(vec) for vec in y_true]

    return f1_score(y_true, y_pred, average='macro')


def main():
    training_data, testing_data, z, zz = import_signal(should_scale=False)

    training_data.balance_categories()

    gc.collect()

    model = get_trained_model(training_data)

    scoring = model.evaluate(testing_data.X, testing_data.y)
    print(scoring)

    f_score = get_f1_score(model, testing_data)
    print(f"F1 score = {str(f_score)}")



