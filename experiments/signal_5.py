"""

Signal Experiment 5.

FFT approach combined with Guassian Naive Bayes

"""

from sklearn.metrics import f1_score
from sklearn.model_selection import GridSearchCV
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import f1_score, accuracy_score, make_scorer
import numpy as np

from data_import.features import n_classes
from data_import.signal import get_fft_pca, plot_data, fft_n_components, import_signal
import cache
from models.DataSet import DataSet

def main():
    training_data = cache.get_dataframe("signal_5_training_data")
    testing_data = cache.get_dataframe("signal_5_testing_data")

    z, zz, training_data, testing_data = \
        import_signal(remove_incomplete_sequences=True,
                      perform_fft=True,
                      should_vectorise=False,
                      should_scale=False)
                                                
    print("Data-set imported... now training classifier(s).")

    clf: GaussianNB = GaussianNB()

    clf.fit(training_data.X, training_data.y)

    y_pred = clf.predict(testing_data.X)
    f1 = f1_score(testing_data.y, y_pred, average='macro')
    print(f"F1 Score = {str(f1)}")

if __name__ == "__main__":
    main()


