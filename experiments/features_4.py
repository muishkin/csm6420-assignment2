"""

Experiment 4

Attempting to generate an AdaBoost classifier.

F1 Score =  0.7556262018065291 (when using AdaBoost)

0.7775972403741841 with balanced categories.

"""
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import f1_score, accuracy_score, make_scorer
import numpy as np

from data_import.features import import_features

def main():
    training_data, testing_data = import_features(should_vectorise=False)

    training_data.balance_categories()

    param_grid = {
        "n_estimators": [600, 700, 900]
    }

    clf: GridSearchCV = GridSearchCV(AdaBoostClassifier(), param_grid, 
                                     n_jobs=3, cv=3,
                                     scoring='accuracy')

    clf.fit(training_data.X, training_data.y)

    best_params = clf.best_params_
    best_score = clf.best_score_
    print(best_params)
    print(f"Gives score: {str(best_score)}")

    y_pred = clf.predict(testing_data.X)
    f1 = f1_score(testing_data.y, y_pred, average='macro')
    print(f"F1 Score = {str(f1)}")

if __name__ == "__main__":
    main()
