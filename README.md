Rob Barry - rob65@aber.ac.uk

Kaggle Submission Team Name - Tîm Mwnci

# Pipenv (Dependency Management)
This project makes use of pipenv for dependency management. Please make sure 
you have installed pipenv before continuing. 

See [Pipenv Installation](https://pipenv.readthedocs.io/en/latest/install/).

Pipenv will also ensure that you are running the correct version of Python for
this project (Python 3.6).

Once you have installed pipenv, install all other project dependencies by 
navigating to the project root directory and executing:

    pipenv sync

# Useful Entry-Points:

* `experiments/features_2.py` - Features Decision Forest Classifier
* `experiments/signal_6.py` - Final raw-signal LSTM classifier.
* `experiments/features_signal_1.py` - Ensemble of features_2 and signal_6.
* `predict.py` - `export_predictions` used for making predictions (export to CSV)


# GPU Acceleration (Basically required)
Certain parts of this project requires the use of 
[CuDNN Acceleration](https://developer.nvidia.com/cudnn).

# Cython (Optional)
Given the slow nature of a number of python data mangling operations, I have 
included a script which uses cython to compile the python scripts. After having
set up pipenv, simple run ./cython.sh to generate all relevant binary files.

# Running code (Python & Cython)
Simple run the following core in the root directory:

    pipenv run python

And then import the relevant experiment you wish to execute:

    >>> import experiments.signal_2 as s2
    Using TensorFlow backend.
    >>> s2.main()
    ...

Alternatively you can run:

    pipenv run python experiments/signal_2.py

However, this approach will not yield the full benefits of the cython 
compilation step.

# Other Notes
Some experiments make use of a cache in `./c` which must exist to work. 
The cache can (inadvisedly) be switched to redis if required.

Some experiments save their models in to the `./models` folder.